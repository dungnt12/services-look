import { Context, Next } from 'koa';

export default async (ctx: Context, next: Next): Promise<void> => {
  try {
    await next();
  } catch (error) {
    ctx.status = error.status || 500;
    ctx.app.emit('error', error, ctx);
  }
};
