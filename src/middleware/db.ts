import { MongoClient } from "mongodb";
import Koa from 'koa';
import dotenv from 'dotenv';
dotenv.config();
const { MONGO_DB, MONGO_CONNECTSTRING } = process.env;

export function init(app: Koa) {
  MongoClient
  .connect(MONGO_CONNECTSTRING, (err, db) => {
    if (err) {
      console.log(`Unable to connect to database: ${MONGO_CONNECTSTRING}`);
    };
    app.context.db = db.db(MONGO_DB);
  }); 
}
