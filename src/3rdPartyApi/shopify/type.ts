export interface Theme {
  id: any;
  name: string;
  created_at: Date;
  updated_at: Date;
  role: string;
  theme_store_id: number;
  previewable: boolean;
  processing: boolean;
  admin_graphql_api_id: string;
}

export interface IThemes {
  themes: Theme[];
}
