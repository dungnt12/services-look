import fetch from 'node-fetch';
import dotenv from 'dotenv';
import delay from 'delay';
import { IThemes } from './type';

dotenv.config();

const { API_VERSION, SERVICE_CDN } = process.env;

const getAllThemes = async (shop: string, accessToken: string): Promise<IThemes> => {
  const fnFetchTheme = async () => {
    const response = await fetch(
      `https://${shop}/admin/api/${API_VERSION}/themes.json`,
      {
        headers: {
          'Content-Type': 'application/json',
          'X-Shopify-Access-Token': accessToken,
        },
        method: 'GET',
      },
    );

    if (!response.ok) {
      await delay(500);
      return fnFetchTheme;
    };
    const resPaser = await response.json();
    return resPaser;
  }

  const res = await fnFetchTheme();

  return res;
}

const getActiveTheme = async (shop: string, accessToken: string): Promise<number> => {
  const themesList = await getAllThemes(shop, accessToken);
  const currentTheme = themesList.themes.filter((theme: any) => theme.role === 'main')[0];
  if (!currentTheme) throw new Error(`Không thể lấy theme active theme ${shop}`);
  if (currentTheme) return currentTheme.id;
};

const createThemeFile = async (
  shop: string,
  accessToken: string,
  fileName: string, // assets/test.txt
  value?: string,
  src?: string,
  themeId?: number,
): Promise<void> => {
  console.log('createThemeFile', fileName);
  let themeID = themeId;
  if (!themeId) {
    themeID = await getActiveTheme(shop, accessToken);
  }

  let contentUploadToTheme;

  if (src !== undefined) {
    contentUploadToTheme = {
      asset: {
        key: fileName,
        src,
      },
    };
  }

  if (value !== undefined) {
    contentUploadToTheme = {
      asset: {
        key: fileName,
        value,
      },
    };
  }

  const assetUploaded = await fetch(
    `https://${shop}/admin/api/${API_VERSION}/themes/${themeID}/assets.json`,
    {
      headers: {
        'Content-Type': 'application/json',
        'X-Shopify-Access-Token': accessToken,
      },
      method: 'PUT',
      body: JSON.stringify(contentUploadToTheme),
    },
  );
  if (!assetUploaded.ok) {
    throw new Error(JSON.stringify(assetUploaded.statusText));
  }
};

const createTheme = async (shop: string, accessToken: string, themeName: string): Promise<number> => {
  const response = await fetch(
    `https://${shop}/admin/api/${API_VERSION}/themes.json`,
    {
      headers: {
        'Content-Type': 'application/json',
        'X-Shopify-Access-Token': accessToken,
      },
      method: 'POST',
      body: JSON.stringify({
        "theme": {
          "name": themeName,
          "src": 'https://cdn.xopify.com/assets/xo-assetstheme.zip'
        }
      })
    },
  );
  if (!response.ok) {
    console.log(response);
    console.log({
      "theme": {
        "name": themeName,
        "src": 'https://cdn.xopify.com/assets/xo-assetstheme.zip'
      }
    });
    throw new Error(`Can't create theme ${shop}`);
  };
  const newTheme = await response.json();
  return newTheme.theme.id;
}

const changeTheme = async (
  shop: string,
  accessToken: string,
  themeID: number,
  newName: string,
): Promise<number> => {
  const response = await fetch(
    `https://${shop}/admin/api/${API_VERSION}/themes/${themeID}.json`,
    {
      headers: {
        'Content-Type': 'application/json',
        'X-Shopify-Access-Token': accessToken,
      },
      method: 'PUT',
      body: JSON.stringify({
        "theme": {
          "name": newName,
          "id": themeID
        }
      })
    },
  );
  if (!response.ok) {
    console.log('err', {
      "url": `https://${shop}/admin/api/${API_VERSION}/themes/${themeID}.json`,
      "token": accessToken,
      "theme": {
        "name": newName,
        "role": "unpublish"
      }
    });
    return -1;
  };
  const newTheme = await response.json();
  return newTheme.theme.id;
}

export {
  getActiveTheme,
  createThemeFile,
  getAllThemes,
  createTheme,
  changeTheme,
};
