import aws from 'aws-sdk';
import dotenv from 'dotenv';
import fs from 'fs';

dotenv.config();

const {
  AWS_REGION,
  AWS_BUCKET_NAME,
  AWS_ACCESSKEYID,
  AWS_SECRETACCESSKEY,
} = process.env;

aws.config.update({
  region: AWS_REGION,
  accessKeyId: AWS_ACCESSKEYID,
  secretAccessKey: AWS_SECRETACCESSKEY,
});

const s3 = new aws.S3({
  apiVersion: '2006-03-01',
  // If you want to specify a different endpoint, such as using DigitalOcean spaces
  // endpoint: new aws.Endpoint("nyc3.digitaloceanspaces.com"),
});

const uploadS3 = async (filePath:string, key:string) => new Promise<{ key: string, url: string }>((resolve, reject) => {
  fs.readFile(filePath, (error: any, data:Buffer) => {
    if (error) reject(error);
    const params = {
      Bucket: AWS_BUCKET_NAME!,
      Key: key,
      Body: data,
    //   ContentType: mime,
    };
    s3.upload(params, (err:Error, img:aws.S3.ManagedUpload.SendData) => {
      if (err) {
        reject(err);
      } else if (img) {
        resolve({ key: img.Key, url: img.Location });
      }
    });
  });
});

const deleteFileS3 = async (listKey:{Key:string}[]) => {
  const params = {
    Bucket: AWS_BUCKET_NAME!,
    Delete: { Objects: listKey },
  };

  s3.deleteObjects(params, (err, data) => {
    if (err) console.log(err, err.stack); // an error occurred
    else console.log(data); // successful response
  });
};

export {
  uploadS3,
  deleteFileS3,
};
