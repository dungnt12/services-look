import fs from 'fs';

const createFolder = (dir: string): void => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true, mode: '775' });
  }
};

export { createFolder };
