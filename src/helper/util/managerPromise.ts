import plimit from 'p-limit';
import _ from 'lodash';

const limit = plimit(2);

class ManagerPromise {
  /**
   * Nên đặt description theo nhóm chức năng
   *  action1/get-data
   *  action1/fetch-data
   */
  private promisesList: {
    name: string;
    promise: Promise<any>;
  }[] = [];

  addPromise<T>(name: string, promise: Promise<T>) {
    this.promisesList.push({ name, promise });
  }

  async run() {
    const { promisesList } = this;
    try {
      await Promise.all(
        promisesList.map(({ promise, name }) => limit(() => {
          console.log(name);
          return promise;
        })),
      );
      this.promisesList = [];
      limit.clearQueue();
    } catch (error) {
      console.log(error.toString());
    }
  }

  clearPromise(prefixName: string) {
    const { promisesList } = this;
    const promiseRemoved = _.remove(promisesList, ({ name }) => {
      const prefix = name.slice(0, name.indexOf(prefixName));
      return prefix === prefixName;
    });
    if (promiseRemoved.length) {
      limit.clearQueue();
    }
  }

  get callStack() {
    const { promisesList } = this;
    return promisesList.map((item) => item.name);
  }
}

const managerPromise = new ManagerPromise();

export { managerPromise };
