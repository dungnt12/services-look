const randomChar = (length: number): string => {
  let result           = '';
  const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for ( let i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const randomChar2 = (length: number): string => {
  let res = '';
  while (length > 0) {
    res += String.fromCharCode(Math.floor(Math.random() * (122 - 65) + 65));
    length -= 1;
  }
  return res;
}

export { randomChar, randomChar2 };

