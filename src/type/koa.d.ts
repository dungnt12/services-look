import { BaseContext } from 'koa';

declare module 'koa' {
  interface BaseContext {
    db: {
      collection: (type: string) => any;
    },
    params: any,
    file: any,
  }
}

