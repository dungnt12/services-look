import {symbolRoutePrefix, Route} from './Route';

/**
 * @prefix('/user')
 */
export function prefix (prefix: string){
  return (target: any) => {
      target.prototype[symbolRoutePrefix] = prefix;
  }
}

/**
 * @router({
 *   method: 'get',
 *   path: '/login/:id'
 * })
 */
export function router (config: {path: string, method: 'get'|'post'|'put'|'patch'|'delete', unless?: boolean}) {
  return (target: any, name: string, value: PropertyDescriptor) => {
      Route.__DecoratedRouters.set({
          target: target,
          path: config.path,
          method: config.method,
          unless: config.unless
      }, target[name]);
  }
}

export function middleware (middleware: Function) {
  return decorate(function(target: any, name: string, descriptor: PropertyDescriptor, middleware: Function){
    target[name] = sureIsArray(target[name]);
    target[name].splice(target[name].length - 1, 0, middleware);
    return descriptor;
  }, sureIsArray(middleware));
}

function decorate (handleDescriptor: Function, entryArgs: Array<Function>) {
  if (isDescriptor(last(entryArgs))) return handleDescriptor(entryArgs);
  else return function () {
      return handleDescriptor(...(arguments as any), ...entryArgs);
  }
}

function last (arr: Array<Function>) {
  return arr[arr.length - 1];
}

function isDescriptor (desc: any) {
  if (!desc || !desc.hasOwnProperty) return false;
  for (let key of ['value', 'initializer', 'get', 'set']) {
    if (desc.hasOwnProperty(key)) return true;
  }
  return false;
}

function sureIsArray (arr: any) {
  return Array.isArray(arr) ? arr : [arr];
}