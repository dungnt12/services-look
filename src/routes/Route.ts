import Koa from 'koa';
import Router from 'koa-router';
import glob from 'glob';
import mount from 'koa-mount';
import serve from 'koa-static';
import path from 'path';

const router = new Router();

export const symbolRoutePrefix:symbol = Symbol("routePrefix");

export class Route {
  static __DecoratedRouters: Map<{
    target: any,
    method: 'get'|'post'|'put'|'patch'|'delete',
    path: string,
    unless?: boolean
  }, Function | Function[]> = new Map();
  
  private router: any;

  private app: Koa;

  constructor(app: Koa){
    this.app = app;
    this.router = router;
  }

  registerRouters(controllerDir: string) {
    glob.sync(path.join(controllerDir, './*.js')).forEach((item: string) => require(item));
    let unlessPath = [];

    Route.__DecoratedRouters.forEach((controller, config) => {
      let controllers = Array.isArray(controller) ? controller : [controller];
      let prefixPath = config.target[symbolRoutePrefix];
      if(prefixPath && (!prefixPath.startsWith('/'))){
        prefixPath += prefixPath;
      }
      const { path }  = config;
      let routerPath = prefixPath + (config.path !== '/' ? config.path : '');
      if(config.unless){
        unlessPath.push(routerPath);
      }
      controllers.forEach((controller) => this.router[config.method](routerPath, controller));
    })

    this.app.use(mount('/files', serve('./files')));
    this.app.use(this.router.routes());
    this.app.use(this.router.allowedMethods());
  }
}