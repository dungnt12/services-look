import * as Koa from 'koa';
import { router, prefix } from '../routes';

@prefix('/api')
export class TestController {
  @router({
    'method': 'get',
    'path': '/haha',
    'unless': true
  })
  async testFun (ctx: Koa.Context): Promise<void> {
    ctx.body = 'hahaha';
  }
}