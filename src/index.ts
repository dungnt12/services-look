import Koa from "koa";
import dotenv from 'dotenv';
import bodyPaser from 'koa-bodyparser';
import { Route } from './routes/Route';
// Middlewares
import errorHandle from './middleware/error';
import * as Database from './middleware/db';

dotenv.config();

const { PORT } = process.env;

const app = new Koa();
const router = new Route(app);

Database.init(app);

app
  .use(errorHandle)
  .use(bodyPaser())

router.registerRouters(`${__dirname}/apis`);

app.listen(PORT, () => {
  console.log(`Service started on http://localhost:${PORT}`);
});
