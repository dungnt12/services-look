import { BaseContext } from 'koa';
import { Collection } from 'mongodb';

const ImagesManagerModel = (ctx: BaseContext): Collection => {
  const ImagesModel = ctx.db.collection('images');

  if (!ImagesModel) throw new Error('Query ImagesManagerModel error');
  return ImagesModel as unknown as Collection;
};

export { ImagesManagerModel };
